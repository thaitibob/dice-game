# Projet Dice Game

Code pour réaliser le projet dice game.

## Pré-requis

Nous donnons les commandes pour Linux, mais vous pouvez évidement trouver facilement les équivalents sur internet pour MAC et WINDOWS.

### Java

Il faut disposer de java 11 pour compiler et lancer le projet.

### JavaFX

Depuis la sortie de Java 11, JavaFX est disponible en tant que module distinct du JDK.
Il faut donc télécharger IMPERATIVEMENT le sdk de javaFX.
Pour plus d'informations pour l'exécution d'un projet Javafx : https://medium.com/@Benlemt/installer-javafx-avec-openjdk-11-ce553ced7b4a
Les mêmes principes sont appliqués sous eclipse, netbeans etc...

### Maven

Pour installer maven :

```
sudo apt install maven
```

### Docker

Installation de docker

```
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
```

Installation de docker-compose

```
sudo apt install docker-compose
```

## Lancement du Projet

### Service de stockage

Il faut tout d'abord lancer les services de base de données.
Pensez à vérifier que vos ports 8081, 8082, 3306, 27017 puis exécutez à la racine du projet :

```
docker-compose up -d
```

Connexion à la BDD MariaDB :
```
# identifiant : root
# mot de passe : admin
http://localhost:8081/
```

Connexion à la BDD MongoDB :
```
# identifiant : dev
# mot de passe : dev
http://localhost:8082/
```

Concernant le stockage CSV celui-ci s'effectuera dans le dossier suivant :

```
# Ce dossier sera crée au lancement du .jar
~/.patrons_conception
```

### Application javaFX

Dans le dossier dicegame et lancer :
```
mvn install
```

Lancer la commande (toujours depuis le dossier dicegame - penser à spécifier son path avec le sdk javafx)

```
java --module-path /path/to/sdkJavaFX/lib --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar target/dice-game-1.0.0-SNAPSHOT-jar-with-dependencies.jar
```