ConnectionFactory produitFactory1 = new MySqlConnectionFactory();
ConnectionFactory produitFactory2 = new MongoConnectionFactory();
ConnectionFactory produitFactory3 = new CsvConnectionFactory();

Connection mysql = null;
Connection mongo = null;
Connection xml = null;

System.out.println("Utilisation de la fabrique maria");
mysql = produitFactory1.getConnection();
mysql.ajoutScore();

System.out.println("Utilisation de la fabrique mongo");
mongo = produitFactory2.getConnection();
mongo.ajoutScore();
mongo.getAllScores();

System.out.println("Utilisation de la fabrique xml");
xml = produitFactory3.getConnection();
xml.ajoutScore();
xml.getAllScores();

--

Article article1 = new Article("Livre : 'Tout savoir sur le pattern Strategy'",25);
Article article2 = new Article("Piano steinway & sons d274",155690);

article1.payer(new PaypalStrategy("idExemple", "passwordExemple"));

//pay by credit card
article2.payer(new CarteDeCreditStrategy( "1234567890123456", "786", "12/15"));