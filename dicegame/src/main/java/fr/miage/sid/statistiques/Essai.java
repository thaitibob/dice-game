package fr.miage.sid.statistiques;

import fr.miage.sid.statistiques.observer.Observer;
import javafx.scene.control.Label;

public class Essai implements Observer {

    private Label essai;

    public Essai(Label l) {
        this.essai = l;
        this.essai.setText("0/10");
    }

    @Override
    public void update(StatsPartie statsPartie) {
        int nb = statsPartie.getPartieTotal();
        this.essai.setText(nb+"/10");
    }

}
