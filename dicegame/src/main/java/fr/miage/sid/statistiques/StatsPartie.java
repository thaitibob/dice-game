package fr.miage.sid.statistiques;

import fr.miage.sid.statistiques.observer.Observable;
import fr.miage.sid.statistiques.observer.Observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class StatsPartie implements Observable {
	private List<Observer> observers;
	
	public StatsPartie(){
		observers = new ArrayList<Observer>();
	}
	
	private int partieGagne = 0;
	private int partieTotal = 0;
	private int cumulPerdu = 0;

	@Override
	public void registerObserver(Observer observer) {
		if(observer != null){
			this.observers.add(observer);
		}
	}

	@Override
	public void notifyObservers() {
		Iterator<Observer> it = observers.iterator();
		while(it.hasNext()){
			Observer observer = it.next();
			observer.update(this);
		}
	}

	@Override
	public void removeObserver(Observer observer) {
		if(observer != null){
			this.observers.remove(observer);
		}
	}

	public int getCumulPerdu() {
		return cumulPerdu;
	}

	public void setCumulPerdu(int cumulPerdu) {
		this.cumulPerdu = cumulPerdu;
	}

	public List<Observer> getObservers() {
		return observers;
	}

	public void setObservers(List<Observer> observers) {
		this.observers = observers;
	}

	public int getPartieGagne() {
		return partieGagne;
	}

	public void setPartieGagne(int partieGagne) {
		this.partieGagne = partieGagne;
	}

	public int getPartieTotal() {
		return partieTotal;
	}

	public void setPartieTotal(int partieTotal) {
		this.partieTotal = partieTotal;
	}

	public void addPartie(boolean partie){
		if(partie){
			this.partieGagne++;
			this.cumulPerdu=0;
		}else{
			this.cumulPerdu++;
		}
		this.partieTotal++;
		this.notifyObservers();
	}

}
