package fr.miage.sid.statistiques.observer;

import fr.miage.sid.statistiques.StatsPartie;

public interface Observer {
	
	public void update(StatsPartie statsPartie);

}
