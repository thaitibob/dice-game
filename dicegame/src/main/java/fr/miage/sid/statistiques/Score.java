package fr.miage.sid.statistiques;

import fr.miage.sid.statistiques.observer.Observer;
import javafx.scene.control.Label;

public class Score implements Observer {

    private Label score;

    public Score(Label l) {
        this.score = l;
        this.score.setText("0 point(s)");
    }

    @Override
    public void update(StatsPartie statsPartie) {
        int points = statsPartie.getPartieGagne()*10;
        this.score.setText(points+" point(s)");
    }
}
