package fr.miage.sid;

import fr.miage.sid.strategy.LancerStrategy;
import javafx.scene.image.Image;

import java.util.HashMap;

public class Dice {

    private HashMap<Integer, Image> dice = new HashMap<>();

    public Dice() {
        for (int i=1; i<7; i++){
            this.dice.put(i,new Image(String.valueOf(getClass().getClassLoader().getResource("images/de"+i+".png"))));
        }
    }

    public Image getImage(int nb){
        return this.dice.get(nb);
    }

    public int roll(LancerStrategy strategy){
        int nombre = strategy.lancer();
        return nombre;
    }

}
