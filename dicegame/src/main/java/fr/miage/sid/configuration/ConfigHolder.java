package fr.miage.sid.configuration;

public class ConfigHolder {

    private Config config;
    private final static ConfigHolder INSTANCE = new ConfigHolder();

    private ConfigHolder() {}

    public static ConfigHolder getInstance() {
        return INSTANCE;
    }

    public void setConfig(Config u) {
        this.config = u;
    }

    public Config getConfig() {
        return this.config;
    }
}
