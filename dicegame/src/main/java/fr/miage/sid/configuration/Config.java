package fr.miage.sid.configuration;

import fr.miage.sid.db.connection.factory.ConnectionFactory;

public class Config {

    private String pseudo;
    private ConnectionFactory stockage;

    public Config(){}

    public Config(String pseudo, ConnectionFactory stockage) {
        this.pseudo = pseudo;
        this.stockage = stockage;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public ConnectionFactory getStockage() {
        return stockage;
    }

    public void setStockage(ConnectionFactory stockage) {
        this.stockage = stockage;
    }
}
