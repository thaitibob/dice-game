package fr.miage.sid;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class StorageScore {

    private String pseudo;
    private String date;
    private int score;
    private String stockage;

    public StorageScore(String pseudo, int score) {
        this.pseudo = pseudo;
        this.score = score;
    }

    public StorageScore(String pseudo, String date, int score, String stockage) {
        this.pseudo = pseudo;
        this.date = date;
        this.score = score;
        this.stockage = stockage;
    }

    public static String DateCourante(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStockage() {
        return stockage;
    }

    public void setStockage(String stockage) {
        this.stockage = stockage;
    }
}
