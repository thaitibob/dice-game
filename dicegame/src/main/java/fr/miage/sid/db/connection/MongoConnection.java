package fr.miage.sid.db.connection;

import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import fr.miage.sid.HighscoreStorage;
import fr.miage.sid.StorageScore;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MongoConnection implements Connection{

    private static final Logger LOGGER = Logger.getLogger(MongoConnection.class.getName());


    private MongoDatabase connection() {
        try {
            MongoClientURI uri = new MongoClientURI("mongodb://root:admin@localhost");
            MongoClient mongoClient = new MongoClient(uri);
            MongoDatabase database = mongoClient.getDatabase("highscore");
            LOGGER.log(Level.INFO, "Connexion MongoDB réussie");
            return database;
        }catch (Exception e){
            LOGGER.log(Level.INFO, "Problème de connexion MongoDB");
        }
        return null;
    }


    @Override
    public boolean checkConnection() {
        if(this.connection()!=null){
            return true;
        }
        return false;
    }

    @Override
    public void ajoutScore(String pseudo, int scoreTotal) {
        MongoDatabase db = this.connection();
        Document document = new Document();
        document.append("pseudo", pseudo);
        document.append("date", StorageScore.DateCourante());
        document.append("valeur", scoreTotal);
        db.getCollection("score").insertOne(document);
        LOGGER.log(Level.INFO, "Donnée insert dans MongoDB");
    }

    @Override
    public void cleanScore() {
        MongoDatabase db = this.connection();
        db.drop();
    }

    @Override
    public ArrayList<HighscoreStorage> getAllScores() {
        ArrayList<HighscoreStorage> st = new ArrayList<>();
        MongoDatabase db = this.connection();
        MongoCollection<Document> collection = db.getCollection("score");
        FindIterable<Document> iterDoc = collection.find();
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            Document d = (Document) it.next();
            HighscoreStorage sc = new HighscoreStorage(d.get("pseudo").toString(), d.get("date").toString(), Integer.parseInt(d.get("valeur").toString()), "Mongo");
            st.add(sc);
        }
        return st;
    }

    @Override
    public String name() {
        return "Mongo";
    }

}
