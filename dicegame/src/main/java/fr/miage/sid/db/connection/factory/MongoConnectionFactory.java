package fr.miage.sid.db.connection.factory;

import fr.miage.sid.db.connection.Connection;
import fr.miage.sid.db.connection.MongoConnection;

public class MongoConnectionFactory implements ConnectionFactory{
    @Override
    public Connection getConnection() {
        return new MongoConnection();
    }
}
