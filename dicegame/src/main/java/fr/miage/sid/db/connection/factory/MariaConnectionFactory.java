package fr.miage.sid.db.connection.factory;


import fr.miage.sid.db.connection.Connection;
import fr.miage.sid.db.connection.MariaConnection;

public class MariaConnectionFactory implements ConnectionFactory {

	@Override
	public Connection getConnection() {
		return new MariaConnection();
	}

}
