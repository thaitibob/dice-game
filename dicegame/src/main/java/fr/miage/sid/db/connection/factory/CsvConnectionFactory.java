package fr.miage.sid.db.connection.factory;

import fr.miage.sid.db.connection.Connection;
import fr.miage.sid.db.connection.CsvConnection;

public class CsvConnectionFactory implements ConnectionFactory{
    @Override
    public Connection getConnection() {
        return new CsvConnection();
    }
}
