package fr.miage.sid.db.connection;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import fr.miage.sid.HighscoreStorage;
import fr.miage.sid.StorageScore;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CsvConnection implements Connection{

    private static final Logger LOGGER = Logger.getLogger(CsvConnection.class.getName());

    private Path connection() {
        String home = "~/.patrons_conception";
        home = System.getProperty("user.home") + home.substring(1);
        Path xml = Paths.get(home, "csv_enregistrements.csv");

        try {
            Files.createDirectories(Paths.get(home));
            if(!Files.exists(xml)){
                Files.createFile(xml);
                File f = new File(String.valueOf(xml));
                try {
                    FileWriter output = new FileWriter(f,true);
                    CSVWriter write = new CSVWriter(output);
                    String[] header = { "pseudo", "date", "valeur" };
                    write.writeNext(header);
                    write.close();
                    LOGGER.log(Level.SEVERE, "Utilisation CSV réussie");
                    return xml;
                } catch (Exception e) {
                    LOGGER.log(Level.SEVERE, "erreur csv ecriture"+e.toString());
                    return null;
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "erreur csv creation "+e.toString());
            return null;
        }
        return xml;
    }


    @Override
    public boolean checkConnection() {
        if(this.connection()!=null){
            return true;
        }
        return false;
    }

    @Override
    public void ajoutScore(String pseudo, int scoreTotal) {
        File f = new File(String.valueOf(this.connection()));
        try {
            FileWriter output = null;
            output = new FileWriter(f, true);
            CSVWriter write = new CSVWriter(output);
            String[] data1 = { pseudo, StorageScore.DateCourante(), String.valueOf(scoreTotal)};
            write.writeNext(data1);
            write.close();
            LOGGER.log(Level.SEVERE, "Insertion CSV réussie");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "erreur insertion CSV"+e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void cleanScore() {
        String home = "~/.patrons_conception";
        home = System.getProperty("user.home") + home.substring(1);

        Path directory = Paths.get(home);

        if(Files.exists(directory)){
            try {
                FileSystemUtils.deleteRecursively(directory);
                LOGGER.log(Level.SEVERE, "suppression csv ok");
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "probleme lors du clean csv"+e.toString());
            }
        }

    }

    @Override
    public ArrayList<HighscoreStorage> getAllScores() {
        ArrayList<HighscoreStorage> st = new ArrayList<>();
        File f = new File(String.valueOf(this.connection()));
        try {
            FileReader filereader = new FileReader(f);

            CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).build();
            String[] nextRecord;

            while ((nextRecord = csvReader.readNext()) != null) {
                String pseudo = nextRecord[0];
                String date = nextRecord[1];
                int valeur = Integer.parseInt(nextRecord[2]);
                HighscoreStorage sc = new HighscoreStorage(pseudo, date, valeur, "CSV");
                st.add(sc);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return st;
    }

    @Override
    public String name() {
        return "Csv";
    }
}
