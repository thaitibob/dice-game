package fr.miage.sid.db.connection.factory;


import fr.miage.sid.db.connection.Connection;

public interface ConnectionFactory {

	Connection getConnection();

}
