package fr.miage.sid.db.connection;

import fr.miage.sid.HighscoreStorage;

import java.util.ArrayList;

public interface Connection {

	public boolean checkConnection();
	public void ajoutScore(String pseudo, int scoreTotal);
	public void cleanScore();
	public ArrayList<HighscoreStorage> getAllScores();
	public String name();

}
