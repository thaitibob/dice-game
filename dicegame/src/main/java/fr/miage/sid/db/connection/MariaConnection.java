package fr.miage.sid.db.connection;

import fr.miage.sid.HighscoreStorage;
import fr.miage.sid.StorageScore;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MariaConnection implements Connection {

	private static final Logger LOGGER = Logger.getLogger(MariaConnection.class.getName());

	private java.sql.Connection connect;
	private String driverName = "org.mariadb.jdbc.Driver";
	private String jdbc = "jdbc:mariadb://";
	private String host = "localhost:";
	private String port = "3306/";
	private String database = "highscore";
	private String url = jdbc + host + port + database;
	private String username = "?user=root";
	private String password = "&password=admin";
	private String variables = "&createDatabaseIfNotExist=true&useSSL=false&allowPublicKeyRetrieval=true";
	private String connection = url + username + password + variables;

	private Statement connection() {
		if (connect == null) {
			try {
				Statement stmt;
				Class.forName(driverName);
				LOGGER.log(Level.INFO, "Driver Class MariaDB Found");
				try {
					connect = DriverManager.getConnection(connection);

					String sql = "CREATE TABLE IF NOT EXISTS `score` ( "
							+ "`id` int(11) NOT NULL AUTO_INCREMENT,"
							+ "`pseudo` varchar(50) DEFAULT NULL,"
							+ "`date` varchar(50) DEFAULT NULL,"
							+ "`valeur` int(11) DEFAULT NULL,"
							+ "PRIMARY KEY (`id`)"
							+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";
					stmt = connect.createStatement();
					stmt.executeUpdate(sql);
					LOGGER.log(Level.INFO, "Connection réussie");
					return stmt;
				} catch (SQLException e) {
					LOGGER.log(Level.SEVERE, "Connection échouée : "+ e);
				}
			} catch (ClassNotFoundException e) {
				LOGGER.log(Level.SEVERE, "Driver Class MariaDB not found : "+ e);
			}
		}
		return null;
	}

	@Override
	public boolean checkConnection() {
		if(this.connection()!=null){
			return true;
		}
		return false;
	}

	@Override
	public void ajoutScore(String pseudo, int scoreTotal) {
		Statement stmt = this.connection();
		String sql = "INSERT INTO score(pseudo,date,valeur) VALUES ('"+pseudo+"','"+StorageScore.DateCourante()+"','"+scoreTotal+"')";
		try {
			stmt.execute(sql);
			LOGGER.log(Level.INFO, "Insertion MariaDB réussie");
		} catch (SQLException throwables) {
			LOGGER.log(Level.SEVERE, "Problème Insertion MariaDB");
		}
	}

	@Override
	public void cleanScore() {
		Statement stmt = this.connection();
		String sql = "DROP TABLE score";
		try {
			stmt.execute(sql);
			LOGGER.log(Level.INFO, "Suppression MariaDB réussie");
		} catch (SQLException throwables) {
			LOGGER.log(Level.SEVERE, "Problème suppression MariaDB");
		}
	}

	@Override
	public ArrayList<HighscoreStorage> getAllScores() {
		ArrayList<HighscoreStorage> st = new ArrayList<>();
		Statement stmt = this.connection();
		String sql = "SELECT * FROM score";
		try {
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next())
			{
				int id = rs.getInt("id");
				String pseudo = rs.getString("pseudo");
				String date = rs.getString("date");
				int valeur = rs.getInt("valeur");
				HighscoreStorage sc = new HighscoreStorage(pseudo, date, valeur, "Maria");
				st.add(sc);
			}
			stmt.close();
			LOGGER.log(Level.INFO, "selection MariaDB réussie");
		} catch (SQLException throwables) {
			LOGGER.log(Level.SEVERE, "Problème séléction MariaDB");
		}
		return st;
	}

	@Override
	public String name() {
		return "MariaDB";
	}
}
