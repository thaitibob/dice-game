package fr.miage.sid.vue;


import fr.miage.sid.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ReglesController implements Initializable {

    @FXML
    public TextArea regle;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.regle.setWrapText(true);
        this.regle.setDisable(true);
        this.regle.setStyle("-fx-opacity: 1;");
    }

    public void handleButtonAction(ActionEvent actionEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("accueil.fxml"));
            Scene scene = new Scene(root);
            App.getStage().setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
