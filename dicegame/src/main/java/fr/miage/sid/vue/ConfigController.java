package fr.miage.sid.vue;

import fr.miage.sid.App;
import fr.miage.sid.configuration.Config;
import fr.miage.sid.configuration.ConfigHolder;
import fr.miage.sid.db.connection.factory.ConnectionFactory;
import fr.miage.sid.db.connection.factory.CsvConnectionFactory;
import fr.miage.sid.db.connection.factory.MariaConnectionFactory;
import fr.miage.sid.db.connection.factory.MongoConnectionFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ConfigController implements Initializable {

    @FXML
    public TextField pseudo;

    @FXML
    public Button btnJouer;

    @FXML
    public RadioButton radioButtonMongo, radioButtonCsv, radioButtonMaria;

    @FXML
    public Label Labeler, titre;

    private ToggleGroup group = new ToggleGroup();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        titre.setAlignment(Pos.CENTER);
        Labeler.setAlignment(Pos.CENTER);

        Labeler.setVisible(false);
        radioButtonMongo.setToggleGroup(group);
        radioButtonCsv.setToggleGroup(group);
        radioButtonMaria.setToggleGroup(group);
        radioButtonMongo.setSelected(true);
    }

    public void handleButtonActionJouer(ActionEvent actionEvent) {
        if(pseudo.getText().length()>2 && pseudo.getText().length()<20){
            String valeur = ((RadioButton) group.getSelectedToggle()).getText();
            ConnectionFactory stockageFactory = null;
            switch(valeur) {
                case "MongoDB":
                    stockageFactory = new MongoConnectionFactory();
                    break;
                case "CSV":
                    stockageFactory = new CsvConnectionFactory();
                    break;
                case "MariaDB":
                    stockageFactory = new MariaConnectionFactory();
                    break;
                default:
                    System.exit(0);
            }

            Config u = new Config(pseudo.getText(),stockageFactory);
            ConfigHolder holder = ConfigHolder.getInstance();
            holder.setConfig(u);

            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getClassLoader().getResource("jouer.fxml"));
                Scene scene = new Scene(root);
                App.getStage().setScene(scene);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            Labeler.setVisible(true);
        }
    }

    public void handleButtonActionRetour(ActionEvent actionEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("accueil.fxml"));
            Scene scene = new Scene(root);
            App.getStage().setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
