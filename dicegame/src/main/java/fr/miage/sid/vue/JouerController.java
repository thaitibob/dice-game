package fr.miage.sid.vue;

import fr.miage.sid.App;
import fr.miage.sid.configuration.Config;
import fr.miage.sid.configuration.ConfigHolder;
import fr.miage.sid.Dice;
import fr.miage.sid.db.connection.Connection;
import fr.miage.sid.statistiques.Essai;
import fr.miage.sid.statistiques.Score;
import fr.miage.sid.statistiques.StatsPartie;
import fr.miage.sid.strategy.LancerStrategy;
import fr.miage.sid.strategy.PriereStrategy;
import fr.miage.sid.strategy.SouffleStrategie;
import fr.miage.sid.strategy.SuiteStrategy;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class JouerController implements Initializable {

    @FXML
    public ImageView boximage_dice1, boximage_dice2;

    @FXML
    public Label pseudoValue, essaieValue, scoreValue, stockageValue;

    @FXML
    public Button  btnRetour, rejouer, btnSouffle, btnSimultane, btnPriere, btnAccueil, btnHighscore;

    @FXML
    public Label tauxSimultane, tauxPriere, tauxSouffle;

    @FXML
    public Label interrogation, perdu, gagner;

    @FXML
    public ImageView perduImg, gagneImg, interrogationImg;

    @FXML
    public Label pseudo,stockage, essaiLabel, score;

    private PriereStrategy priere;
    private SouffleStrategie souffleStrategie;
    private SuiteStrategy suiteStrategy;

    private Score sc;
    private Essai essai;
    private StatsPartie stats;

    private Dice dice;

    private int nb_image_dice1, count_dice1, nb_image_dice2, count_dice2;
    private Connection connection;
    private int nb_parties = 0;
    private int score_total = 0;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        pseudo.setAlignment(Pos.BASELINE_RIGHT);
        stockage.setAlignment(Pos.BASELINE_RIGHT);
        essaiLabel.setAlignment(Pos.BASELINE_RIGHT);
        score.setAlignment(Pos.BASELINE_RIGHT);

        this.perdu.setVisible(false);
        this.gagner.setVisible(false);
        this.rejouer.setVisible(false);
        this.perduImg.setVisible(false);
        this.gagneImg.setVisible(false);

        this.stats  = new StatsPartie();
        this.sc     = new Score(this.scoreValue);
        this.essai  = new Essai(this.essaieValue);

        this.suiteStrategy      = new SuiteStrategy(this.tauxSimultane);
        this.priere             = new PriereStrategy(this.tauxPriere);
        this.souffleStrategie   = new SouffleStrategie(this.tauxSouffle);

        this.stats.registerObserver(this.suiteStrategy);
        this.stats.registerObserver(this.priere);
        this.stats.registerObserver(this.souffleStrategie);
        this.stats.registerObserver(this.sc);
        this.stats.registerObserver(this.essai);

        this.dice = new Dice();

        ConfigHolder holder = ConfigHolder.getInstance();
        Config u = holder.getConfig();
        this.connection = u.getStockage().getConnection();
        this.pseudoValue.setText(u.getPseudo());
        this.stockageValue.setText(connection.name());


        this.boximage_dice1.setImage(dice.getImage(1));
        this.boximage_dice2.setImage(dice.getImage(1));
        this.nb_image_dice1 = 1;
        this.nb_image_dice2 = 1;
    }

    public void write(LancerStrategy strategie) {

        this.count_dice1=0;
        this.count_dice2=0;
        PauseTransition pause = new PauseTransition(Duration.millis(100));
        pause.setOnFinished(event ->{
            this.count_dice1++;
            this.count_dice2++;

            if (this.count_dice1<10 && this.count_dice2<10) {
                int randomIntDice1 = 0;
                int randomIntDice2 = 0;
                randomIntDice1 = ((int) (Math.random()*(7 - 1))) + 1;
                randomIntDice2 = ((int) (Math.random()*(7 - 1))) + 1;
                this.boximage_dice1.setImage(dice.getImage(randomIntDice1));
                this.boximage_dice2.setImage(dice.getImage(randomIntDice2));
                this.nb_image_dice1 = randomIntDice1;
                this.nb_image_dice2 = randomIntDice2;
                pause.play();
            }else if(this.count_dice1==10 && this.count_dice2==10){
                int randomIntDice1 = dice.roll(strategie);
                int randomIntDice2 = dice.roll(strategie);;

                this.nb_image_dice1 = randomIntDice1;
                this.nb_image_dice2 = randomIntDice2;
                pause.play();
            }else{
                if(this.nb_image_dice1 + this.nb_image_dice2 == 7){
                    this.stats.addPartie(true);
                    this.boximage_dice1.setImage(dice.getImage(this.nb_image_dice1));
                    this.boximage_dice2.setImage(dice.getImage(this.nb_image_dice2));
                }else{
                    int randomIntDice1, randomIntDice2;
                    do {
                        randomIntDice1 = ((int) (Math.random()*(7 - 1))) + 1;
                        randomIntDice2 = ((int) (Math.random()*(7 - 1))) + 1;
                    }while (randomIntDice1+randomIntDice2==7);
                    this.boximage_dice1.setImage(dice.getImage(randomIntDice1));
                    this.boximage_dice2.setImage(dice.getImage(randomIntDice2));
                    this.stats.addPartie(false);
                }
                this.actionButton(false);
                this.endPartie();
            }
        });
        pause.play();
    }

    public void handleButtonActionRetour(ActionEvent actionEvent) {
        this.exit("config.fxml");
    }

    private void exit(String file){
        Alert alert = new Alert(Alert.AlertType.WARNING,
                "Annuler la partie",
                ButtonType.YES, ButtonType.NO);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.YES){
            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getClassLoader().getResource(file));
                Scene scene = new Scene(root);
                App.getStage().setScene(scene);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void actionButton(boolean disable){
        this.btnRetour.setDisable(disable);
        this.btnSouffle.setDisable(disable);
        this.btnSimultane.setDisable(disable);
        this.btnPriere.setDisable(disable);
        this.btnAccueil.setDisable(disable);
        this.btnHighscore.setDisable(disable);
    }

    private void endPartie(){
        if(this.stats.getPartieTotal()==10){
            this.interrogationImg.setVisible(false);
            this.interrogation.setVisible(false);
            if(this.stats.getPartieGagne()>=3){
                this.gagneImg.setVisible(true);
                this.gagner.setVisible(true);
            }else{
                this.perduImg.setVisible(true);
                this.perdu.setVisible(true);
            }
            this.rejouer.setVisible(true);
            int points = this.stats.getPartieGagne()*10;
            this.connection.ajoutScore(this.pseudoValue.getText(),points);
            this.tauxSimultane.setVisible(false);
            this.tauxPriere.setVisible(false);
            this.tauxSouffle.setVisible(false);
            this.btnSouffle.setDisable(true);
            this.btnSimultane.setDisable(true);
            this.btnPriere.setDisable(true);
        }
    }

    public void handleButtonPriere(ActionEvent actionEvent) {
        this.actionButton(true);
        write(this.priere);
    }

    public void handleButtonSimultane(ActionEvent actionEvent) {
        this.actionButton(true);
        write(this.suiteStrategy);
    }

    public void handleButtonSouffle(ActionEvent actionEvent) {
        this.actionButton(true);
        write(this.souffleStrategie);
    }

    public void handleButtonActionRejouer(ActionEvent actionEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("jouer.fxml"));
            Scene scene = new Scene(root);
            App.getStage().setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleButtonActionHighscore(ActionEvent actionEvent) {
        this.exit("highscore.fxml");
    }

    public void handleButtonActionAccueil(ActionEvent actionEvent) {
        this.exit("accueil.fxml");
    }
}
