package fr.miage.sid.vue;

import fr.miage.sid.App;
import fr.miage.sid.HighscoreStorage;
import fr.miage.sid.db.connection.factory.ConnectionFactory;
import fr.miage.sid.db.connection.factory.CsvConnectionFactory;
import fr.miage.sid.db.connection.factory.MariaConnectionFactory;
import fr.miage.sid.db.connection.factory.MongoConnectionFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class HighscoreController implements Initializable {

    @FXML private TableView<HighscoreStorage> table;
    @FXML private TableColumn<HighscoreStorage, String> pseudo;
    @FXML private TableColumn<HighscoreStorage, String> date;
    @FXML private TableColumn<HighscoreStorage, Integer> score;
    @FXML private TableColumn<HighscoreStorage, String> stockage;

    public ObservableList<HighscoreStorage> list = FXCollections.observableArrayList();

    ConnectionFactory MongostockageFactory;
    ConnectionFactory MariastockageFactory;
    ConnectionFactory CsvstockageFactory;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        MongostockageFactory = new MongoConnectionFactory();
        ArrayList<HighscoreStorage> st = MongostockageFactory.getConnection().getAllScores();
        for(HighscoreStorage s : st){
            list.add(s);
        }

        MariastockageFactory = new MariaConnectionFactory();
        ArrayList<HighscoreStorage> st2 = MariastockageFactory.getConnection().getAllScores();
        for(HighscoreStorage s : st2){
            list.add(s);
        }

        CsvstockageFactory = new CsvConnectionFactory();
        ArrayList<HighscoreStorage> st3 = CsvstockageFactory.getConnection().getAllScores();

        for(HighscoreStorage s : st3){
            list.add(s);
        }

        pseudo.setCellValueFactory(new PropertyValueFactory<HighscoreStorage, String>("pseudo"));
        date.setCellValueFactory(new PropertyValueFactory<HighscoreStorage, String>("date"));
        score.setCellValueFactory(new PropertyValueFactory<HighscoreStorage, Integer>("score"));
        stockage.setCellValueFactory(new PropertyValueFactory<HighscoreStorage, String>("stockage"));
        score.setSortType(TableColumn.SortType.DESCENDING);
        table.setItems(list);
        table.getSortOrder().add(score);
    }

    public void handleButtonActionRetour(ActionEvent actionEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("accueil.fxml"));
            Scene scene = new Scene(root);
            App.getStage().setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleButtonActionClean(ActionEvent actionEvent) {
        MongostockageFactory.getConnection().cleanScore();
        MariastockageFactory.getConnection().cleanScore();
        CsvstockageFactory.getConnection().cleanScore();
        list = FXCollections.observableArrayList();
        table.setItems(list);
    }
}
