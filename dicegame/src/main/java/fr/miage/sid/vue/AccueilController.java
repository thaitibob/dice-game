package fr.miage.sid.vue;

import fr.miage.sid.App;
import fr.miage.sid.db.connection.factory.ConnectionFactory;
import fr.miage.sid.db.connection.factory.CsvConnectionFactory;
import fr.miage.sid.db.connection.factory.MariaConnectionFactory;
import fr.miage.sid.db.connection.factory.MongoConnectionFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.TextAlignment;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AccueilController implements Initializable{

    @FXML
    public Button btnJouer,btnHighscore;

    @FXML
    public Label fail,success;

    @FXML
    public Label titre;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        titre.setAlignment(Pos.CENTER);
        fail.setAlignment(Pos.CENTER);
        success.setAlignment(Pos.CENTER);

        ConnectionFactory MongostockageFactory;
        ConnectionFactory MariastockageFactory;
        ConnectionFactory CsvstockageFactory;
        MongostockageFactory = new MongoConnectionFactory();
        MariastockageFactory = new MariaConnectionFactory();
        CsvstockageFactory = new CsvConnectionFactory();
        if(!MongostockageFactory.getConnection().checkConnection() || !MariastockageFactory.getConnection().checkConnection() || !CsvstockageFactory.getConnection().checkConnection()){
            fail.setVisible(true);
            success.setVisible(false);
            btnHighscore.setDisable(true);
            btnJouer.setDisable(true);
        }else{
            fail.setVisible(false);
            success.setVisible(true);
        }

    }

    public void handleButtonActionRegle(ActionEvent actionEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("regles.fxml"));
            Scene scene = new Scene(root);
            App.getStage().setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleButtonActionJouer(ActionEvent actionEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("config.fxml"));
            Scene scene = new Scene(root);
            App.getStage().setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleButtonActionHighscore(ActionEvent actionEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("highscore.fxml"));
            Scene scene = new Scene(root);
            App.getStage().setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
