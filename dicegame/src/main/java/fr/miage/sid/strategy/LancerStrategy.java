package fr.miage.sid.strategy;

public interface LancerStrategy {
    public int lancer();
}
