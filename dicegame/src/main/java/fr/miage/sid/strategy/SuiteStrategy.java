package fr.miage.sid.strategy;

import fr.miage.sid.statistiques.observer.Observer;
import fr.miage.sid.statistiques.StatsPartie;
import javafx.scene.control.Label;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class SuiteStrategy implements LancerStrategy, Observer {

    private final double defaut = 2;
    private double nombre;
    private Label taux;

    public SuiteStrategy(Label l) {
        this.nombre = this.defaut;
        this.taux = l;
        this.taux.setText(taux()+"%");
    }

    @Override
    public int lancer() {
        ArrayList<Integer> de = new ArrayList<>();
        for(int i=1;i<7;i++){
            if(i == 3 || i==4){
                for(int j=0;j<this.nombre;j++){
                    de.add(i);
                }
            }else{
                de.add(i);
            }
        }
        int randomIntDice = ((int) (Math.random()*(de.size()-1)));
        return de.get(randomIntDice);
    }

    private BigDecimal taux(){
        double nb_total = (this.nombre*2)+4;
        BigDecimal proba = new BigDecimal(100*2*((this.nombre/nb_total)*(this.nombre/nb_total)));
        BigDecimal roundOff = proba.setScale(2, RoundingMode.HALF_EVEN);
        return roundOff;
    }

    @Override
    public void update(StatsPartie statsPartie) {
        statsPartie.getCumulPerdu();
        this.nombre=this.defaut+statsPartie.getCumulPerdu()*2;
        this.taux.setText(taux()+"%");
    }
}
