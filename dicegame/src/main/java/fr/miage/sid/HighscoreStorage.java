package fr.miage.sid;

public class HighscoreStorage {

    private final String pseudo;
    private final String date;
    private final int score;
    private final String stockage;

    public HighscoreStorage(String pseudo, String date, int score, String stockage) {
        this.pseudo = pseudo;
        this.date = date;
        this.score = score;
        this.stockage = stockage;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getDate() {
        return date;
    }

    public int getScore() {
        return score;
    }

    public String getStockage() {
        return stockage;
    }
}
